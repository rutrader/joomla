jQuery(function($){
	if($.datepicker)
	{
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
		'Июл','Авг','Сен','Окт','Ноя','Дек'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Нед',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
                changeMonth: true,
		changeYear: true,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
	}
	
	if($.timepicker)
	{
	$.timepicker.regional['ru'] = {
	timeOnlyTitle: 'Выберите время',
	timeText: 'Время',
	hourText: 'Часы',
	minuteText: 'Минуты',
	secondText: 'Секунды',
	millisecText: 'миллисекунды',
	currentText: 'Сейчас',
	closeText: 'Закрыть',
	showSecond: true,
	timeFormat: 'hh:mm:ss',
	ampm: false};
        
	$.timepicker.setDefaults($.timepicker.regional['ru']);
	}
});

var itform_types = new Object();
itform_types['number'] = {regexp:"/^[0-9]*?\.?[0-9]+$/", errmsg:"Значение должно соответствовать формату числа с плавующей точкой.", enablechars: "/^[0-9+\-\.]$/"};
itform_types['login'] = {regexp:"/^[a-z@\.-0-9]+$/i", errmsg:"Логин должен состоять из латинских букв, цифр, дефиса, точки, собачки. Другие символы использовать нельзя.", enablechars: "/^[a-zA-Z0-9\-_\.@]$/"};
itform_types['password'] = {regexp:"/^\S+$/", errmsg:"Пароль не должен содержать пробелов и табуляций", enablechars: "/^\S$/"};
itform_types['phone'] = {regexp:"/^[+()0-9 ,\-добext\.]+$/iu", errmsg:"Телефон нужно указывать в формате +7 (495) 988-3040.", enablechars: "/^[+()0-9 ,\-добext\.]$/iu"};
itform_types['date'] = {regexp:"/^[12][7-90-3]\d\d-(?:0\d|1[0-2])-(?:[0-2]\d|3[01])$/", errmsg:"Дата должна быть в формате гггг-мм-дд", enablechars: "/^[0-9\-]$/"};
itform_types['datetime'] = {regexp:"/^[12][7-90-3]\d\d-(?:0\d|1[0-2])-(?:[0-2]\d|3[01]) (?:[01]\d|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", errmsg:"Дата должна быть в формате гггг-мм-дд чч:мм:сс", enablechars: "/^[ 0-9\-:]$/"};
itform_types['time'] = {regexp:"/^(?:[01]\d|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", errmsg:"Время должно быть в формате чч:мм:сс", enablechars: "/^[0-9:]$/"};
itform_types['integer'] = {regexp:"/^-?\d+$/", errmsg:"Целое число может состоять из знака минус и цифр.", enablechars: "/^[0-9\-]$/"};
itform_types['unsigned'] = {regexp:"/^\d+$/", errmsg:"Целое неотрицательное число может состоять только из цифр.", enablechars: "/^[0-9]$/"};
itform_types['bankaccount'] = {regexp:"/^\d{20}$/", errmsg:"Двадцать цифр должно быть без каких-либо разделителей.", enablechars: "/^[0-9]$/"};
itform_types['negative'] = {regexp:"/^-\d+$/", errmsg:"Целое отрицательное число должно состоять из знака минус и цифр.", enablechars: "/^[0-9\-]$/"};
itform_types['ip'] = {regexp:"/^[0-9%\.]+$/", errmsg:"IP должен иметь формат 192.168.1.%", enablechars: "/^[0-9%\.]$/"};
itform_types['inn'] = {regexp:"/^\d{10}(\d\d){0,1}$/", errmsg:"ИНН организации должно состоять из 10 цифр, индивидуального предпринимателя -- из 12 цифр.", enablechars: "/^\d$/"};
itform_types['kpp'] = {regexp:"/^\d{9}$/", errmsg:"КПП должно быть из 9 цифр.", enablechars: "/^\d$/"};
itform_types['ogrn'] = {regexp:"/^\d{13}(\d\d){0,1}$/", errmsg:"ОГРН - 13 цифр должен сожержать, ОГРН ИП -- 15.", enablechars: "/^\d$/"};
itform_types['bik'] = {regexp:"/^\d{9}$/", errmsg:"БИК должен содержать 9 цифр.", enablechars: "/^\d$/"};

function itform_Error(error, value, restriction)
{
 switch(error)
 {
  case 'minlength': return 'Значение «'+value+'» должно содержать минимум '+restriction+' символов';
  case 'regexp': return 'Значение «'+value+'» должно удовлетворять условиям регулярного выражения '+restriction;
  case 'minvalue': return 'Значение «'+value+'»  должно быть не меньше '+restriction;
  case 'maxvalue': return 'Значение «'+value+'»  должно быть не больше '+restriction;
  case 'from_gt_to': return 'Значение ОТ не может быть больше значения ДО';
  case 'to_lt_from': return 'Значение ДО не может быть меньше значения ОТ';
  case 'radiorequired': return 'Необходимо выбрать один из вариантов';
  case 'minselected': return 'Должно быть выбрано не меньше '+restriction + ' элементов';
  case 'maxselected': return 'Должно быть выбрано не больше '+restriction + ' элементов';
  case 'fileext': return 'Выбран файл с расширением «' + value + '» Но вы должны выбрать файл с одним из следующих расширений ' + restriction;
 }  
}